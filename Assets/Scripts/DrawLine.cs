﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class DrawLine : MonoBehaviour {
	
	private  LineRenderer line; 
	private List <LineRenderer> rendererLines = new List <LineRenderer>();

	private Vector3 touchPos;
	public Material material;
	private int currLines = 0; //number of lines drawn

	private List <ButtonHolder> buttonHolders = new List<ButtonHolder>();
	public List <AudioClip> temporaryAudioClipList = new List<AudioClip> ();
	public List <AudioClip> realAudioClipList = new List<AudioClip> ();
	private List <ButtonHolder> temporaryButtonHolders  = new List<ButtonHolder>();

	public int nieudaneProby;
	public Text countText;
	//public Connection polaczenie;

	public void Init()
	{
		nieudaneProby = 0;
		SetCountText ();
//		polaczenie = GameObject.Find ("Polaczenie").GetComponent<Connection>();

		//DontDestroyOnLoad ();

		for (int i = 0; i < buttonHolders.Count; i++) 
		{
			realAudioClipList.Add (buttonHolders [i].audioSource.clip);
		}
	}
 
	void Update ()
	{
		if (Input.touchCount > 0) 
		{
			if(Input.GetTouch (0).phase == TouchPhase.Began)
			{	
				if(rendererLines.Count==0)
				{
					createLine();
				}

				touchPos = Camera.main.ScreenToWorldPoint(Input.GetTouch (0).position);
				touchPos.z = 0;
				//set the start point and end point of the line renderer
				line.SetPosition(0,touchPos);
				line.SetPosition(1,touchPos);
			}

			else if((Input.GetTouch (0).phase == TouchPhase.Ended) && rendererLines.Count!=0)
			{
				if ((temporaryButtonHolders.Count != realAudioClipList.Count) && (temporaryButtonHolders.Count > 1)) 
				{
					nieudaneProby++;
					SetCountText ();
					WrongFading.Instance.StartFade ();
				}

				temporaryAudioClipList.Clear ();
				temporaryButtonHolders.Clear ();
				touchPos = Camera.main.ScreenToWorldPoint(Input.GetTouch (0).position);
				touchPos.z = 0;

				//set the end point of the line renderer to current mouse position
				line.SetPosition(1,touchPos);

				for (int i = 0; i < rendererLines.Count; i++) 
				{
					Destroy (rendererLines[i]);
				}
				rendererLines.Clear ();
			}
			//if mouse button is held clicked and line exists
			else if((Input.GetTouch(0).phase == TouchPhase.Moved) && rendererLines.Count!=0)
			{
				touchPos = Camera.main.ScreenToWorldPoint(Input.GetTouch (0).position);
				touchPos.z = 0;
				//set the end position as current position but dont set line as null as the mouse click is not exited
				line.SetPosition(1, touchPos);

				for (int i=0; i<buttonHolders.Count;i++)
				{
					if (buttonHolders [i].circleCollider2D.OverlapPoint (touchPos))
					{
						if ((temporaryButtonHolders.Count < buttonHolders.Count) && !(temporaryButtonHolders.Contains(buttonHolders[i])))
						{
							temporaryButtonHolders.Add (buttonHolders [i]);
						}

						Porownaj ();
						//set the end point of the line renderer to current mouse position
						line.SetPosition(1,touchPos);
						//set line as null once the line is created

						//Destroy (line);

						createLine();

						//set the start point and end point of the line renderer
						line.SetPosition(0,touchPos);
						line.SetPosition(1,touchPos);
					}
				}
			}
		}
	}


	//method to create line
	private void createLine()
	{
		//create a new empty gameobject and line renderer component
		line = new GameObject("Line"+currLines).AddComponent<LineRenderer>();
		//assign the material to the line
		line.material = material;
		//set the number of points to the line
		line.SetVertexCount(2);
		//set the width
		line.SetWidth(0.3f,0.3f);
		//render line to the world origin and not to the object's position
		line.useWorldSpace = true;
		rendererLines.Add (line);
	}

	public void AddButton(GameObject button) 
	{
		buttonHolders.Add (new ButtonHolder (button));
	}

	private class ButtonHolder 
	{
		public GameObject button;
		public CircleCollider2D circleCollider2D;
		public AudioSource audioSource;

		public ButtonHolder (GameObject button)
		{
			this.button = button;
			this.circleCollider2D = button.GetComponent<CircleCollider2D> ();
			this.audioSource = button.GetComponent<AudioSource> ();
		}
	}
		
	public void Porownaj()
	{
		if (temporaryButtonHolders.Count == realAudioClipList.Count)
		{
			
			for (int i = temporaryButtonHolders.Count-1; i >=0 ; i--)
			{
				temporaryAudioClipList.Add (temporaryButtonHolders [i].audioSource.clip);
				temporaryButtonHolders.RemoveAt(i);
			}

			temporaryAudioClipList.Reverse ();	

			int k = 0;
			for (int i = 0; i < temporaryAudioClipList.Count; i++) {
				if (temporaryAudioClipList [i] == realAudioClipList [i]) {
					k++;
				}
			}

			if (k == temporaryAudioClipList.Count) 
			{
				for (int i = 0; i < rendererLines.Count; i++) 
				{
					rendererLines[i].SetColors(new Color (0,1,0,1),new Color (0,1,0,1));
				}
					
				//StartCoroutine (WaitForResult ());


				string scena = SceneManager.GetActiveScene().name.ToString();
				//GameObject.Find ("Polaczenie").GetComponent<Connection>().PostResults(scena,nieudaneProby);
				Debug.Log ("Poszloooooooo" + scena + " " + nieudaneProby);

				GoodFading.Instance.StartFade ();
				FadingScene.Instance.StartFade("Win");

			}	
			else 
				{
				for (int i = 0; i < rendererLines.Count; i++) 
				{
					rendererLines[i].SetColors(new Color (1,0,0,1),new Color (1,0,0,1));
				}
				 //StartCoroutine (WaitForResult ());
				 nieudaneProby++;
				 SetCountText ();
				 WrongFading.Instance.StartFade ();
				}
		}
	}

	IEnumerator WaitForResult()
	{
		yield return new WaitForSeconds (5f);
	}

	void SetCountText ()
	{
		countText.text = "Liczba prób: " + nieudaneProby.ToString ();
	}


}
