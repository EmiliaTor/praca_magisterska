﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class IntervalSample : MonoBehaviour {

	public GameObject buttonIntervalSamplePrefab;
	public Sprite buttonIntervalSampleSprite;

	private GameObject button;

	private AudioClip first;
	private AudioClip second;

	public List <AudioClip> audioSprites = new List<AudioClip>();

	public float speed;
	public int roznica;
	private int a;
	private int b;

	private bool zmienna;

	public Vector3 pozycja;


	void Start () 
	{
		zmienna = false;
		button = Instantiate (buttonIntervalSamplePrefab, pozycja, Quaternion.identity) as GameObject;
		button.GetComponent<SpriteRenderer> ().sprite = buttonIntervalSampleSprite;
		IntervalControler intervalControler = GetComponent<IntervalControler> ();

		a = Random.Range (0, audioSprites.Count);
		b = Random.Range(0, audioSprites.Count);

		roznica = Mathf.Abs(a-b);

		while ((roznica == 0) || (roznica > 12))
		{
			b = Random.Range(0, audioSprites.Count);
			roznica = Mathf.Abs(a-b);
		}
			
		first = audioSprites[a];
		second = audioSprites[b];
		Debug.Log ("Roznica" + roznica);
		intervalControler.Init ();
	}

	void Update()
	{
		if(Input.touchCount > 0) 
		{
			Vector3 touchRelative = Camera.main.ScreenToWorldPoint (Input.GetTouch (0).position);

			if (((Input.GetTouch (0).phase == TouchPhase.Began) || (Input.GetTouch (0).phase == TouchPhase.Moved)) && (zmienna == false) && ((button.GetComponent<CircleCollider2D> ().OverlapPoint (touchRelative)))) 
			{
				StartCoroutine (PlayMusicSample());
						
				zmienna = true;
			}
			if ((Input.GetTouch (0).phase == TouchPhase.Ended)||(!(button.GetComponent<CircleCollider2D>().OverlapPoint (touchRelative))))
				zmienna = false;
		}
	}


	IEnumerator PlayMusicSample()
	{
		button.GetComponent<AudioSource>().clip = first;
		button.GetComponent<AudioSource>().Play ();
		yield return new WaitForSeconds (speed);
		button.GetComponent<AudioSource>().clip = second;
		button.GetComponent<AudioSource>().Play ();
		yield return new WaitForSeconds (speed);
	}



}