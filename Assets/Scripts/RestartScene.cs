﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class RestartScene : MonoBehaviour {

	public GameObject buttonRestartScenePrefab;
	public Sprite buttonRestartSceneSprite;
	private GameObject button;
	private bool zmienna;
	public string restartSceneString;

	// Use this for initialization
	void Awake () {
		zmienna = false;
	}

	void Start()
	{
		button = Instantiate(buttonRestartScenePrefab);
		button.GetComponent<SpriteRenderer> ().sprite = buttonRestartSceneSprite;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(Input.touchCount > 0) 
		{
			Vector3 touchRelative = Camera.main.ScreenToWorldPoint (Input.GetTouch (0).position);

			if ((Input.GetTouch (0).phase == TouchPhase.Began) && (zmienna == false) && (button.GetComponent<CircleCollider2D> ().OverlapPoint (touchRelative)))
			{
				SceneManager.LoadScene (restartSceneString);

				zmienna = true;
			}
			if ((Input.GetTouch (0).phase == TouchPhase.Ended)||(!(button.GetComponent<CircleCollider2D>().OverlapPoint (touchRelative))))
				zmienna = false;
		}
	
	}
}
