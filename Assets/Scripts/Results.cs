﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Results {

	public string rodzajGry { get; set; }
	public int level { get; set; }
	public int nieudaneProby { get; set; }
	public string deviceId { get; set; }

	public Hashtable toHashtable() {
		Hashtable hashtable = new Hashtable ();

		if (this.deviceId != null)
			hashtable.Add ("deviceId", this.deviceId);
		if (this.rodzajGry != null)
			hashtable.Add ("rodzajGry", this.rodzajGry);
		
		hashtable.Add ("level", this.level);
		hashtable.Add ("nieudaneProby", this.nieudaneProby);

		return hashtable;
	}
}
