﻿using UnityEngine;
using System;
using System.Collections; // arraylist
using System.Collections.Generic; // list
using Random = UnityEngine.Random;

public class ChordButtons : MonoBehaviour {

	public GameObject buttonPrefab;
	public GameObject boundsObject;

	public int buttonCount;

	public List <Sprite> buttonSprites = new List<Sprite>();
	public List <AudioClip> audioSprites = new List<AudioClip>();

	public List<Vector3> pozycja = new List<Vector3>();


	void Start(){
		ButtonTouchControler buttonTouchControler = GetComponent<ButtonTouchControler> ();
		ChordSample buttonChordSample = GetComponent<ChordSample> ();
		ChordChecker buttonChordChecker = GetComponent<ChordChecker> ();

		for (int i = 0; i < buttonCount; i++) 
		{
			GameObject button = Instantiate(buttonPrefab, pozycja[i], Quaternion.identity) as GameObject;
			button.GetComponent<SpriteRenderer>().sprite = buttonSprites[Random.Range(0, buttonSprites.Count)];
			button.GetComponent<AudioSource>().clip = audioSprites[i];

			buttonTouchControler.AddButton (button);
			buttonChordSample.AddButton (button);
			buttonChordChecker.AddButton (button);
		}
		buttonChordSample.Init ();
	}
}