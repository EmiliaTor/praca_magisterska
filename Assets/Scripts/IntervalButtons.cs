﻿using UnityEngine;
using System;
using System.Collections; // arraylist
using System.Collections.Generic; // list
using Random = UnityEngine.Random;

public class IntervalButtons : MonoBehaviour {

	public List<GameObject> buttonPrefab = new List<GameObject>();
	public List <Sprite> buttonSprites = new List<Sprite>();
	public List<Vector3> pozycja = new List<Vector3>();

	public int buttonCount;

	void Start()
	{
		IntervalControler intervalControler = GetComponent<IntervalControler> ();

		for (int i = 0; i < buttonCount; i++) 
		{
			GameObject button = Instantiate(buttonPrefab[i], pozycja[i], Quaternion.identity) as GameObject;
			button.GetComponent<SpriteRenderer>().sprite = buttonSprites[i];
			intervalControler.AddButton (button);
		}
	}
}