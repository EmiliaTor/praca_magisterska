﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;


public class IntervalControler : MonoBehaviour {

	public List <ButtonHolder> buttonHolders = new List<ButtonHolder>();
	private bool zmienna;
	private int roznicaInterval;
	public int nieudaneProby;

	public Text countText;

	// Use this for initialization
	public void Init () {
		nieudaneProby = 0;
		zmienna = false;
		roznicaInterval = GetComponent<IntervalSample> ().roznica;
	}
	
	// Update is called once per frame
	void Update () {
		
		if(Input.touchCount > 0) 
		{
			for (int i = 0; i < buttonHolders.Count; i++) 
			{
				Vector3 touchRelative = Camera.main.ScreenToWorldPoint (Input.GetTouch (0).position);
				if ((Input.GetTouch (0).phase == TouchPhase.Began) && (zmienna == false)&& (buttonHolders [i].circleCollider2D.OverlapPoint (touchRelative)))
				{
					if (i + 1 == roznicaInterval) 
					{
						//jest dobrze

						string scena = SceneManager.GetActiveScene().name.ToString();
						//GameObject.Find ("Polaczenie").GetComponent<Connection>().PostResults(scena,nieudaneProby);

						GoodFading.Instance.StartFade ();
						FadingScene.Instance.StartFade("Win");
					} 
					else 
					{
						//jest Źle
						nieudaneProby++;
						SetCountText ();
						WrongFading.Instance.StartFade ();	
					}
					zmienna = true;
					//dodać zmianę sprite'a na podświetlony
				}
				if ((Input.GetTouch (0).phase == TouchPhase.Ended) || (!(buttonHolders [i].circleCollider2D.OverlapPoint (touchRelative)))) 
				{
					zmienna = false;
					// tu dodać zmianę sprite'a na podstawowy
				}
			}
		}
	
	}



	public void AddButton(GameObject button) 
	{
		buttonHolders.Add (new ButtonHolder (button));
	}
		
	public class ButtonHolder 
	{
		public GameObject button;
		public CircleCollider2D circleCollider2D;

		public ButtonHolder (GameObject button)
		{
			this.button = button;
			this.circleCollider2D = button.GetComponent<CircleCollider2D> ();
		}
	}

	void SetCountText ()
	{
		countText.text = "Liczba prób: " + nieudaneProby.ToString ();
	}
}
