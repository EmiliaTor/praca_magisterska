﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ChordSample : MonoBehaviour {

	private List <ButtonHolder> buttonHolders = new List<ButtonHolder>();
	public List <ButtonHolder> buttonSamples = new List<ButtonHolder>();
	private ButtonHolder tempButton;


	public GameObject buttonChordSamplePrefab;
	public Sprite buttonChordSampleSprite;
	private GameObject button;

	private bool zmienna;
	public Vector3 pozycja;
	public int sounds;

	void Start () {
		zmienna = false;
		button = Instantiate (buttonChordSamplePrefab, pozycja, Quaternion.identity) as GameObject;
		button.GetComponent<SpriteRenderer> ().sprite = buttonChordSampleSprite;

	}

	public void Init()
	{
		buttonSamples.Clear ();

		int i = 0;
		while (i < sounds) {
			tempButton = buttonHolders[Random.Range(0, buttonHolders.Count)];
			if (!buttonSamples.Contains (tempButton)) {
				buttonSamples.Add (tempButton);
				i++;
			}
		}
		//tu dodac init
		GetComponent<ChordChecker> ().Init();

	}

	void Update()
	{
		if(Input.touchCount > 0) 
		{
			Vector3 touchRelative = Camera.main.ScreenToWorldPoint (Input.GetTouch (0).position);

			if (((Input.GetTouch (0).phase == TouchPhase.Began) || (Input.GetTouch (0).phase == TouchPhase.Moved)) && (zmienna == false) && ((button.GetComponent<CircleCollider2D> ().OverlapPoint (touchRelative)))) 
			{
				for (int i = 0; i < buttonSamples.Count; i++) 
				{
					buttonSamples [i].audioSource.Play ();
				}
				zmienna = true;
			}
			if ((Input.GetTouch (0).phase == TouchPhase.Ended)||(!(button.GetComponent<CircleCollider2D>().OverlapPoint (touchRelative))))
				zmienna = false;
		}
	}
		
	public void AddButton(GameObject button) 
	{
		buttonHolders.Add (new ButtonHolder (button));
	}

	public class ButtonHolder 
	{
		public GameObject button;
		public CircleCollider2D circleCollider2D;
		public AudioSource audioSource;
		public SpriteRenderer spriteRenderer;
		public bool buttonState;

		public ButtonHolder (GameObject button)
		{
			this.button = button;
			this.circleCollider2D = button.GetComponent<CircleCollider2D> ();
			this.audioSource = button.GetComponent<AudioSource> ();
			this.buttonState = false;
			this.spriteRenderer = button.GetComponent<SpriteRenderer>();
		}
	}


}