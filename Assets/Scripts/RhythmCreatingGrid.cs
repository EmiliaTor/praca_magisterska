﻿using UnityEngine;
using System;
using System.Collections; // arraylist
using System.Collections.Generic; // list
using Random = UnityEngine.Random;

public class RhythmCreatingGrid : MonoBehaviour {

	public GameObject buttonPrefab;
	public GameObject boundsObject;

	public int buttonCount;
	public float factor; // mnożnik kolumn, wierszy

	private float d; // największy wspolny dzielnik
	private float playAreaX;
	private float playAreaY;
	private float columns;
	private float rows;
	private float max;

	public List <Sprite> buttonSprites = new List<Sprite>();
	private List <Vector3> phantomGridPositions = new List <Vector3>();

	private Vector3 realGridPosition;

	public List <AudioClip> audioSprites = new List<AudioClip>();
	private List <GameObject> audioButtons;

	// Inicjalizacja
	void Awake () {


		playAreaX = boundsObject.GetComponent<BoxCollider2D> ().size.x;
		playAreaY = boundsObject.GetComponent<BoxCollider2D> ().size.y;

		d = NWD (playAreaX, playAreaY);

		columns = factor * playAreaX / d;
		rows = factor * playAreaY / d;

		//warunek na max petli
		if (columns >= rows)
			max = columns;
		else
			max = rows;

		InitialiseList ();
	}

	void Start(){
		RhythmButtonTouchControler buttonTouchControler = GetComponent<RhythmButtonTouchControler> ();
		DrawLine buttonDrawLine = GetComponent<DrawLine> ();
		//MusicSample buttonMusicSample = GetComponent<MusicSample> ();
		RhythmSample buttonRhythmSample = GetComponent<RhythmSample> ();


		for (int i = 1; i <= buttonCount; i++) 
		{
			Vector3 randomPosition = RandomPosition();

			GameObject button = Instantiate(buttonPrefab, randomPosition, Quaternion.identity) as GameObject;
			button.GetComponent<SpriteRenderer>().sprite = buttonSprites[Random.Range(0, buttonSprites.Count)];
			button.GetComponent<AudioSource>().clip = audioSprites[Random.Range(0, audioSprites.Count)];

			buttonTouchControler.AddButton (button);
			buttonDrawLine.AddButton (button);
			//buttonMusicSample.AddButton (button);
			buttonRhythmSample.AddButton (button);
		}

		buttonDrawLine.Init ();
	}

	void InitialiseList ()
	{
		phantomGridPositions.Clear ();

		for(float phantomX = (-columns+1)/2; phantomX < columns/2; phantomX++)
			//for(float phantomX = (-columns+1)/2; phantomX <= columns/2; phantomX++)
		{
			//w każdej kolumnie pętla przez oś y (rows).
			for(float  phantomY = (-rows+1)/2; phantomY < (rows-5)/2; phantomY++)
				//for(float  phantomY = (-rows+1)/2; phantomY <= (rows)/2; phantomY++)
			{
				phantomGridPositions.Add (new Vector3(phantomX, phantomY, 0f));
			}
		}
	}

	//RandomPosition zwraca random position z listy gridPositions.
	Vector3 RandomPosition ()
	{
		//deklaracja randomIndex, ustawienie wartosci jako random z numerow pomiedzy 0 i liczba elementow z List gridPositions.
		int randomIndex = Random.Range (0, phantomGridPositions.Count);

		//deklaracja zmiennej randomPosition, której przypisywana jest pozycja o losowym indeksie z gridPositions
		Vector3 tempRandomPosition = phantomGridPositions[randomIndex];
		List <int> tempList = new List <int>();
		tempList.Clear ();

		//usuwa z listy element, by nie mogl byc ponownie uzyty
		phantomGridPositions.RemoveAt (randomIndex);

		for (int i=0; i<phantomGridPositions.Count; i++) 
		{
			//usuwanie w pionie i poziomie
			if ((phantomGridPositions [i].x == tempRandomPosition.x) || (phantomGridPositions [i].y == tempRandomPosition.y)) 
			{	
				tempList.Add(i);
				phantomGridPositions.RemoveAt (i);
				i = 0;
			}

			for (int a = 1; a <= max; a++) {
				//usuwanie ukosnych - prawy gorny
				if ((phantomGridPositions [i].x == tempRandomPosition.x + a) && (phantomGridPositions [i].y == tempRandomPosition.y - a)) {	
					tempList.Add (i);
					phantomGridPositions.RemoveAt (i);
					i = 0;
				}

				//usuwanie ukosnych - prawy dolny
				if ((phantomGridPositions [i].x == tempRandomPosition.x + a) && (phantomGridPositions [i].y == tempRandomPosition.y + a)) {	
					tempList.Add (i);
					phantomGridPositions.RemoveAt (i);
					i = 0;
				}

				//usuwanie ukosnych - lewy gorny
				if ((phantomGridPositions [i].x == tempRandomPosition.x-a) && (phantomGridPositions [i].y == tempRandomPosition.y-a)) {	
					tempList.Add (i);
					phantomGridPositions.RemoveAt (i);
					i = 0;
				}

				//usuwanie ukosnych - lewy dolny
				if ((phantomGridPositions [i].x == tempRandomPosition.x-a) && (phantomGridPositions [i].y == tempRandomPosition.y+a)) {	
					tempList.Add (i);
					phantomGridPositions.RemoveAt (i);
					i = 0;
				}
			}
		}

		realGridPosition.x = (playAreaX / columns) * tempRandomPosition.x;
		realGridPosition.y = (playAreaY / rows) * tempRandomPosition.y;
		realGridPosition.z = 0;

		if (phantomGridPositions.Count == 0)
			Debug.Log ("NIE MIESCI SIE!!!!");

		return realGridPosition;
	}

	//Wyszukuje nawiększy wspólny dzielnik (długości i szerokości)
	private float NWD(float a,float b)
	{
		float c;
		while (b != 0) 
		{
			c = a % b;
			a = b;
			b = c;
		}
		return a;
	}

}