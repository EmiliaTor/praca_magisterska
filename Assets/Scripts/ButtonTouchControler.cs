﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ButtonTouchControler : MonoBehaviour {
	public bool inRange;

	public List <ButtonHolder> buttonHolders = new List<ButtonHolder>();

	public List <AudioClip> buttonSound = new List<AudioClip>();
	public bool zmienna;
	public int grateraz;
	// Inicjalizacja
	void Awake () {
		zmienna = false;
	}
	// Update wywolywany raz na ramke czasu
	void Update ()
	{
		PlaySound ();
	}

	public void AddButton(GameObject button) 
	{
		buttonHolders.Add (new ButtonHolder (button));
	}

	[System.Serializable]
	public class ButtonHolder 
	{
		public GameObject button;
		public CircleCollider2D circleCollider2D;
		public AudioSource audioSource;

		public ButtonHolder (GameObject button)
		{
			this.button = button;
			this.circleCollider2D = button.GetComponent<CircleCollider2D> ();
			this.audioSource = button.GetComponent<AudioSource> ();
		}
	}

	private void PlaySound()
	{
		if(Input.touchCount > 0) 
		{
			for (int i = 0; i < buttonHolders.Count; i++) 
			{
				Vector3 touchRelative = Camera.main.ScreenToWorldPoint (Input.GetTouch (0).position);
				if (((Input.GetTouch (0).phase == TouchPhase.Began) || (Input.GetTouch (0).phase == TouchPhase.Moved)) && (zmienna == false)&& ((buttonHolders [i].circleCollider2D.OverlapPoint (touchRelative))&&(inRange)))
				{
					buttonHolders [i].audioSource.Play();
					zmienna = true;
					grateraz = i;
				}
				if ((Input.GetTouch (0).phase == TouchPhase.Ended)||(!((buttonHolders [i].circleCollider2D.OverlapPoint (touchRelative))&&(inRange))&&(grateraz==i)))
					zmienna = false;
			}
		}
	}

}