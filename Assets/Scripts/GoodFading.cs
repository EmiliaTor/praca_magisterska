﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GoodFading : MonoBehaviour 
{
	// -- Singleton Structure
	protected static GoodFading mInstance;
	public static GoodFading Instance
	{
		get
		{
			if (mInstance == null)
			{
				GameObject tempObj = new GameObject();
				mInstance = tempObj.AddComponent<GoodFading>();
				Destroy(tempObj);
			}
			return mInstance;
		}
	}

	// -- Fade Canvas
	public Canvas FadePrefab;
	Canvas FadeSprite;

	// -- Variables
	float f_Alpha = 1.0f, f_Speed = 10f; // zmiana z 1f na 10f

	// -- Instantiate
	void InstantiateFade()
	{
		if (FadePrefab != null) 
			FadeSprite = Instantiate(FadePrefab, FadePrefab.transform.position, Quaternion.identity) as Canvas;
	}

	// -- Initialisation
	void Start()
	{
		// -- Set Instance
		mInstance = this;
	}

	IEnumerator Delete()
	{
		yield return new WaitForSeconds (0.1f);
		Destroy (FadeSprite.gameObject);
		FadeSprite = null;
	}


	// -- Start Fading [THIS IS THE FUNCTION U CALL TO TOGGLE FADING]
	public void StartFade()
	{

		// -- Instantiate if Canvas doesn't exist
		if (FadeSprite == null)
			InstantiateFade();

		// -- Set Default Color
		Color DefaultColor = FadeSprite.GetComponentInChildren<Image>().color;

		if (FadeSprite.GetComponentInChildren<Image> ().color.a < 1.0f) 
		{	
			f_Alpha += Time.deltaTime * f_Speed;
		}

		FadeSprite.GetComponentInChildren<Image> ().color = new Color (DefaultColor.r, DefaultColor.g, DefaultColor.b, 0.0f);

		if (FadeSprite.GetComponentInChildren<Image> ().color.a > 0.0f) 
		{
			f_Alpha -= Time.deltaTime * f_Speed;
		}
		else 
			StartCoroutine (Delete ());

		FadeSprite.GetComponentInChildren<Image> ().color = new Color (DefaultColor.r, DefaultColor.g, DefaultColor.b, 1.0f);

	}
}