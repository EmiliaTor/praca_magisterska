﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class LevelSuggestion {

	public string rodzajGry;
	public int level;

	public string mergeToNameScene() {
		string test;
		test = rodzajGry + "Level" + level.ToString ();
		return test;
	}
}
