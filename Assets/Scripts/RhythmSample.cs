﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RhythmSample : MonoBehaviour {

	private List <ButtonHolder> buttonHolders = new List<ButtonHolder>();
	// Use this for initialization

	public GameObject buttonRhythmSamplePrefab;
	public Sprite buttonRhythmSampleSprite;
	private bool zmienna;
	private GameObject button;

	void Awake () {
		zmienna = false;
	}


	void Start()
	{
		button = Instantiate(buttonRhythmSamplePrefab);
		button.GetComponent<SpriteRenderer> ().sprite = buttonRhythmSampleSprite;
		//	buttonAudioSource = button.GetComponent<AudioSource> ();
	}

	void Update()
	{
		if(Input.touchCount > 0) 
		{
			Vector3 touchRelative = Camera.main.ScreenToWorldPoint (Input.GetTouch (0).position);

			if (((Input.GetTouch (0).phase == TouchPhase.Began) || (Input.GetTouch (0).phase == TouchPhase.Moved)) && (zmienna == false) && ((button.GetComponent<CircleCollider2D> ().OverlapPoint (touchRelative)))) 
			{
				StartCoroutine (PlayMusicSample ());
				zmienna = true;
			}
			if ((Input.GetTouch (0).phase == TouchPhase.Ended)||(!(button.GetComponent<CircleCollider2D>().OverlapPoint (touchRelative))))
				zmienna = false;
		}
	}

	IEnumerator PlayMusicSample()
	{
		for (int i = 0; i < buttonHolders.Count; i++) 
		{
			buttonHolders [i].audioSource.Play ();
			yield return new WaitForSeconds (buttonHolders [i].audioSource.clip.length);
			//buttonHolders [i].audioSource.clip.length
		}
	}

	public void AddButton(GameObject button) 
	{
		buttonHolders.Add (new ButtonHolder (button));
	}

	private class ButtonHolder 
	{
		public GameObject button;
		public CircleCollider2D circleCollider2D;
		public AudioSource audioSource;

		public ButtonHolder (GameObject button)
		{
			this.button = button;
			this.circleCollider2D = button.GetComponent<CircleCollider2D> ();
			this.audioSource = button.GetComponent<AudioSource> ();
		}
	}
}