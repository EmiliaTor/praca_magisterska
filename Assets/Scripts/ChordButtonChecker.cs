﻿using UnityEngine;
using System.Collections;

public class ChordButtonChecker : MonoBehaviour {

	public GameObject buttonChordButtonCheckerPrefab;
	public Sprite buttonChordButtonCheckerSpriteA;
	public Sprite buttonChordButtonCheckerSpriteB;
	private GameObject button;
	private ChordChecker buttonChordChecker;
	private bool zmienna;
	public Vector3 pozycja;


	void Awake () 
	{
		zmienna = false;
	}
	// Use this for initialization
	void Start () 
	{
		button = Instantiate (buttonChordButtonCheckerPrefab, pozycja, Quaternion.identity) as GameObject;
		button.GetComponent<SpriteRenderer> ().sprite = buttonChordButtonCheckerSpriteA;
		buttonChordChecker = GetComponent<ChordChecker> ();
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(Input.touchCount > 0) 
		{
			Vector3 touchRelative = Camera.main.ScreenToWorldPoint (Input.GetTouch (0).position);

			if ((Input.GetTouch (0).phase == TouchPhase.Began) && (zmienna == false) && (button.GetComponent<CircleCollider2D> ().OverlapPoint (touchRelative)))
			{
				buttonChordChecker.Check ();
				zmienna = true;
			}
			if ((Input.GetTouch (0).phase == TouchPhase.Ended) || (!(button.GetComponent<CircleCollider2D> ().OverlapPoint (touchRelative)))) 
			{
				zmienna = false;
			}
		}
	}
}
