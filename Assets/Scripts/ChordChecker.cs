﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;


public class ChordChecker : MonoBehaviour {

	private List <ButtonHolder> buttonHolders = new List<ButtonHolder>();
	private List <AudioClip> audioClipSampleList = new List<AudioClip> ();
	private List <AudioClip> audioClipHolderList = new List<AudioClip> ();

	private GameObject button;

	public Sprite buttonSpriteA;
	public Sprite buttonSpriteB;

	public Vector3 pozycja;

	public int nieudaneProby;
	public Text countText;


	// Use this for initialization
	public void Init () 
	{
		nieudaneProby = 0;
		audioClipSampleList.Clear ();
		int max = GetComponent<ChordSample> ().buttonSamples.Count;

		for (int i = 0; i < max; i++) 
		{
			audioClipSampleList.Add (GetComponent<ChordSample> ().buttonSamples[i].audioSource.clip);
		}
	}

	// Update is called once per frame
	void Update () 
	{
		if (Input.touchCount > 0) 
		{
			for (int i = 0; i < buttonHolders.Count; i++) {

				Vector3 touchRelative = Camera.main.ScreenToWorldPoint (Input.GetTouch (0).position);
				
				if ((Input.GetTouch (0).phase == TouchPhase.Began) && (buttonHolders[i].circleCollider2D.OverlapPoint (touchRelative))) 
				{	
					if (buttonHolders[i].buttonState==false) 
					{
						buttonHolders [i].spriteRenderer.sprite = buttonSpriteB;
						Debug.Log ("jest false chce zmienic na true");
						//tu zmiana sprite na podswietlony/zaznaczony
						buttonHolders[i].buttonState=true;
					} 
					else 
					{
						//tu zmiana sprite na zwykly
						Debug.Log ("jest true chce zmienic na false");
						buttonHolders [i].spriteRenderer.sprite= buttonSpriteA;
						buttonHolders[i].buttonState=false;
					}
				}
			}
		}	
	}

	public void AddButton(GameObject button) 
	{
		buttonHolders.Add (new ButtonHolder (button));
	}

	private class ButtonHolder 
	{
		public GameObject button;
		public CircleCollider2D circleCollider2D;
		public AudioSource audioSource;
		public SpriteRenderer spriteRenderer;
		public bool buttonState;

		public ButtonHolder (GameObject button)
		{
			this.button = button;
			this.circleCollider2D = button.GetComponent<CircleCollider2D> ();
			this.audioSource = button.GetComponent<AudioSource> ();
			this.buttonState = false;
			this.spriteRenderer = button.GetComponent<SpriteRenderer>();
		}
	}

	public void Check()
	{
		int c = 0;
		int d = 0;

		audioClipHolderList.Clear ();

		for (int i = 0; i < buttonHolders.Count; i++) 
		{
			if (buttonHolders [i].buttonState == true) 
			{
				audioClipHolderList.Add (buttonHolders [i].audioSource.clip);
				c++;
			}
		}

		if (c != audioClipSampleList.Count) 
		{
			//ŹLE
			nieudaneProby++;
			SetCountText ();
			WrongFading.Instance.StartFade ();
		}

		else //if (c == audioClipSampleList.Count) 
		{
			for (int i = 0; i < audioClipSampleList.Count; i++) 
			{
				if (audioClipHolderList.Contains(audioClipSampleList[i]))
					d++;
			}
			
			if (d==c)
			{
				string scena = SceneManager.GetActiveScene().name.ToString();
				//GameObject.Find ("Polaczenie").GetComponent<Connection>().PostResults(scena,nieudaneProby);
				//super 
				GoodFading.Instance.StartFade ();
				FadingScene.Instance.StartFade("Win");
			}
			else
			{
				nieudaneProby++;
				SetCountText ();
				WrongFading.Instance.StartFade ();	
			}
		}
	}

	void SetCountText ()
	{
		countText.text = "Liczba prób: " + nieudaneProby.ToString ();
	}



}
